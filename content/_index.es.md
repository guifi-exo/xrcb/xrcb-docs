---
title: XRCB docs
subtitle: o porque la documentación de la XRCB es de tu ayuda
comments: false
---

## como editar los docs

La web de documentación usa [GitLab Pages](https://about.gitlab.com/features/pages/)
/ [Hugo](https://gohugo.io).
Estamos usando por ahora el tema `beautifulhugo` para construir la página.
Edita el contenido de  `/content/_index.md` para cambiar esto. Borra `/content/_index.md`
para que esto desaparezca.

Para editar contenido añade documentos en el directorio `/content/` y enlazalos en el menu `config.toml`

Tenemos que elegir un tema para esta web, ahora mismo está usando [Learn](https://themes.gohugo.io/hugo-theme-learn/):

*   [Hugo-book](https://themes.gohugo.io/hugo-book/) pro:  limpio, claro // cons: 
*   [Learn](https://themes.gohugo.io/hugo-theme-learn/) pro: muy limpio, moderno, claro muy atractivo,// cons:

---
title: Grafica 
subtitle: elementos gráficos de la XRCB
comments: false 
---

Recopilatorio y compendio de imagen gráfica de la [XRCB](https://xrcb.cat)

### Imagotipo
#### Versión estándar

![XRCB logo rectangular vectorial](../../images/XRCB-logo-horizontal-vectorial.svg)

![XRCB logo rectangular inverted vectorial](../../images/XRCB-logo-horizontal-vectorial-invert.svg)

#### Pantallas y/o tamaño pequeño

![XRCB logo rectangular pequeño vectorial](../../images/XRCB-logo-mobile-vectorial.svg)

![XRCB logo rectangular pequeño vectorial inverted](../../images/XRCB-logo-mobile-vectorial-invert.svg)

### Logomarca / icono

![XRCB logomarca icono vectoriali](../../images/XRCB-logo-x-icon.svg)

![XRCB logo rectangular vectorial](../../images/XRCB-logo-x-icon-invert.svg)

### Banner

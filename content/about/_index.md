---
title: Acerca de...
chapter: false
weight: 5
---

Esta es la página de ayuda de la XRCB y pretendemos:

- Enseñar
- Cooperar
- Ayudar
- Documentar

Los procesos que van y vienen en el desarrollo de la infraestructura, uso y mantenimiento de la XRCB.

### Nuestras historia

#### La Xarxa

La XRCB és un projecte *no comercial* del programa Cultura Viva de l’Ajuntament de Barcelona en col·laboració amb la comunitat guifi.net /exo.cat de Barcelona.

Más información en [xrcb.cat](https://xrcb.cat/ca/qui-som/)

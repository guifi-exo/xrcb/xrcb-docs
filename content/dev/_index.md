---
title: Desarrollo
subtitle: desarrollos de la plataforma xrcb.cat
weight: 10
comments: false
---

###  Desarrollos de la plataforma xrcb.cat 
#### [Wordpress development manual]({{< ref "dev_wordpress.md" >}})
#### [Git manual]({{< ref "dev_git.md" >}})

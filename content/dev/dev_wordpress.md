---
title: Wordpress development manual
subtitle: howto setup and modify Wordpress themes and plugins for XRCB
weight: 20
comments: false
---

## Overview

### Theme and plugins

We developed a custom theme and a bunch of plugins to run [XRCB website](https://xrcb.cat/). The custom developments are on github:

- [xrcb-theme](https://gitlab.com/guifi-exo/xrcb/xrcb-theme)
- [xrcb-plugin](https://gitlab.com/guifi-exo/xrcb/xrcb-plugin)
- [xrcb-player-wp-plugin](https://gitlab.com/guifi-exo/xrcb/xrcb-player-wp-plugin)
- [xrcb-player](https://gitlab.com/guifi-exo/xrcb/xrcb-player)
- [xrcb-api](https://gitlab.com/guifi-exo/xrcb/xrcb-api)
- [xrcb-carousel](https://gitlab.com/guifi-exo/xrcb/xrcb-carousel)

Additionaly at least these Wordpress plugins are needed to run the site:
- [Advanced Custom Fields](https://es.wordpress.org/plugins/advanced-custom-fields/): Custom post types, like _radio_, _podcast_ and _resources_ are based on it.
- [WP Multilang](https://es.wordpress.org/plugins/wp-multilang/): Multilanguge plugin to offer site in _catalan_, _spanish_ and _english_.
- [Easy Appointments](https://es.wordpress.org/plugins/easy-appointments/): Programmable calendar is based on this plugin.

## Used JavaScript libraries

- [jQuery hashchange event v1.3](http://benalman.com/projects/jquery-hashchange-plugin/)
- [Leaflet 1.3.1](http://leafletjs.com/)
- [DataTables 1.10.16](http://www.datatables.net/)

### Custom Post Types

#### UML diagram

![UML diagram](../../images/dev_wordpress-uml.png)

#### radio

Custom post type _radio_ has the following fields (out of date):

- Title
- Category
- Neighbourhood
- Year of foundation
- Short history
- License
- Web
- Mail
- Address: This fields includes address as a text field plus latitude and longitude. It's the format used by Google Maps to make it compatible with the optional [ACF Google Maps field](https://www.advancedcustomfields.com/resources/google-map/):
```array( 'address' => 'xxx', 'lat' => 'xxx', 'lng' => 'xxx' )```

#### podcast

Custom post type _podcast_ has the following fields (out of date):

- Title
- Description
- Radio
- Date of broadcasting
- File MP3

#### resources

Custom post type _resource_ has the following fields:

- Title
- URL

### Page templates

- `Radios JSON`: Exports all registers of post type `radio` to Json. ([Example output of proyect XRCB](https://xrcb.cat/ca/radios-json/))
- `Radios GeoJSON`: Exports all registers of post type `radio` to GeoJson using latitude and longitude. ([Example output of project XRCB](https://xrcb.cat/ca/radios-geojson/))
- `Radios Datatables`: Shows all registers of post type `radio` as a [data table](http://www.datatables.net/).
- `Podcasts JSON`: Exports all registers of post type `podcast` to Json. ([Example output of project XRCB](https://xrcb.cat/ca/podcasts-json/))
- `Podcasts FEED`: Exports all registers of post type `podcast` as podcast XML RSS feed. ([Example output of radio La Comunitaria](https://xrcb.cat/ca/radio/la-comunitaria/))
- `Podcasts Datatables`: Shows all registers of post type `podcast` as a [data table](http://www.datatables.net/).
- `Resources JSON`: Exports all registers of post type `resource` to Json.
- `Resources Datatables`: Shows all registers of post type `resource` as a [data table](http://www.datatables.net/).
- `Home`: Hides content window, typically used for home pages which directly want to show a map without any floating popup.
- `Calendar JSON`: Exports all calendar events to Json.

### APIs



### Installation

1. Download and install Wordpress theme _xrcb-theme_.
2. Install and activate plugin [Advanced Custom Fields v5](https://www.advancedcustomfields.com/). This plugin is necesarry for editing content of type `radio`. If you want to avoid this plugin, you have to implement a custom form for editing content of type `radio`. 
3. Create content for custom post type `radio`.
4. Create page /radios-geojson using page template `Radios GeoJSON` (used by Leaflet to generate markers on the background map).
5. Create page /home using page template `Home` and define this page as default start page.

Optional to activate view radios datatable:
1. Create page /radios-json using page template `Radios JSON`.
2. Create page /radios-list using page template `Radios Datatable`.

Optional to activate view podcasts datatable:
1. Create page /podcasts-json using page template `Podcasts JSON`.
2. Create page /podcasts-list using page template `Podcasts Datatable`.


---
title: Git development manual
subtitle: how we organize our git repositories
weight: 20
comments: false
---

## Introducción

El workflow en resumen es:
*_Trabajar en test y test2 -> cuando funciona pasar a preprod -> cuando funciona pasar a prod._*

Para conesguir esta forma de workflow gitificado, tenemos a disposición los estos servidores y repositorios git:

- xrcb.cat: `git branch prod`, estado de xrcb.cat producción
- preprod.xrcb.cat: `git branch preprod`, estado de preproducción. Sirve para:
  - ensayo general antes de pasar a prod
  - pequeños bugfixes que hay que pasar rápido a producción
- test2.xrcb.cat: `git branch test2` para cambios en desarrollo
- test.xrcb.cat: está en forma de git, pero no siempre está sync y aquí se producen cambios libres

## Development workflow

    prod      ----------------------------
                \   /   \           /
    preprod      ---     \     -----
                          \   /
    test                   ---

Para meter los cambios de `preprod` a `prod`, se hace lo siguiente:

**Trabajamos** siempre en **local**, **despues** aplicamos **cambios** en remoto al **server**.

#### Preparar cambios en `preprod` en _local_
  - entrar en preprod `git checkout preprod`
  - ponerte al día `git pull`
  - comprobar que lo que tienes es lo que quieres en preprod :D es decir, trabajar :D

#### Aplicar cambios `preprod` al _server_
En el **server**, poner https://preprod.xrcb.cat al dia:
  - ssh al server
  - `cd /var/www/html/DIRECTORIO` dentro del theme o dentro del plugin
  - `git checkout preprod`
  - `git pull`

Hasta aqui es para ver en que punto estás de **preprod**

#### Aplicar cambios a `prod` en _local_

Si todo es correcto entonces quieres poner al día `prod` en **local** para ello:
  - `git checkout preprod`
  - `git merge prod`. Si hay **errores** o **avisos** hay que arreglar inconsistencias, **volver** a punto de [preparar cambios en preprod](#preparar-cambios-en-preprod-en-_local_), **si todo** va **bien** entonces **seguimos**.
  - `cd /var/www/html/DIRECTORIO` dentro del theme o dentro del plugin
  - `git checkout prod`
  - `git merge preprod` y arreglar inconsistencias si las hay. cuando estemos felices.
  - `git push`

#### Aplicar cambios de `prod` al _server_
Momento en el que aplicamos los cambios a https://xrcb.cat
  - ssh al server
  - `cd /var/www/html/DIRECTORIO` dentro del theme o dentro del plugin
  - `git checkout prod`
  - `git pull`



Referencia: https://www.atlassian.com/git/tutorials/comparing-workflows/gitflow-workflow

### Resetear `test2` con la info de `preprod`

para poder probar cosas si queresmo olvidar lo que hay en `test2`y ponerlo idéntico a `preprod`

en local

```
git checkout preprod
git branch -d test2
git branch test2
git checkout test2
git push origin test2 --force
```

luego hacer ssh al server y actualizar los files para aplicar cambios:

ir al directorio de wordpress de test2

```
git push origin test2 --force
git reset --hard origin/test2
```

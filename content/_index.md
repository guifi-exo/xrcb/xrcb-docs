---
title: XRCB docs
subtitle: o perquè la documentació de la XRCB és de la teva ajuda
comments: false
---

## Com editar els docs

La web de documentació fa servir [GitLab Pages](https://about.gitlab.com/features/pages/)
/ [Hugo](https://gohugo.io).
Estem usant ara com ara el tema `beautifulhugo` per a construir la pàgina.
Edita el contingut de `/content/_index.md` per a canviar això. Esborra `/content/_index.md`
perquè això desaparegui.

Per a editar contingut afegeix documents en el directori `/content/` i enllaça'ls en el menú `config.toml`

Hem de triar un tema per a aquesta web, ara mateix està usant [Learn](https://themes.gohugo.io/hugo-theme-learn/):

*   [Hugo-book](https://themes.gohugo.io/hugo-book/) pro:  net, clar // cons: 
*   [Learn](https://themes.gohugo.io/hugo-theme-learn/) pro: molt net, modern, clar molt atractiu,// cons: 

#### Anexo estadísticas streaming

Las estadísticas no se están recogiendo de forma ordenada.

Por el momento tenemos un script que se deja el tiempo que dure el evento

```
#!/bin/sh -e
# pull data every 1 minutes -> https://stackoverflow.com/questions/24503494/run-curl-command-every-5-seconds/24503528#24503528
while sleep $((1*60)); do
  curl -s http://xrcb.cat:8000/mounts.xsl -o "$(date +'%Y-%m-%d_%H-%M-%S')"
done
```

y luego se analizan los datos resultantes con el siguiente script en R

```
library(data.table)
library(jsonlite)
library(ggplot2)


HOME = Sys.getenv('HOME')
mypath = paste0(HOME, '/temp/radiorambles/radiorambles2')

setwd(mypath)

myfiles <- list.files(mypath, pattern = "*")

dt <- data.table()
for (n in 1:length(myfiles)) {
  day = strsplit(myfiles[n],'_')[[1]][1]
  time_raw = strsplit(myfiles[n],'_')[[1]][2]
  time = gsub('-',':',time_raw)
  date_str = paste(day,time)
  json_file <- fromJSON(myfiles[n])
  listeners = as.numeric(json_file$mounts$`/main.mp3`$listeners)
  # why POSIXlt https://www.neonscience.org/resources/learning-hub/tutorials/dc-convert-date-time-posix-r
  dt <- rbind(dt, data.table(day, time, date_str, date = as.POSIXlt(date_str), listeners))
}

#ggplot(dt, aes(x = date, y = listeners)) + geom_line()
ggplot(dt, aes(x = date, y = listeners)) + geom_smooth()
ggsave('radiorambles.png')
```

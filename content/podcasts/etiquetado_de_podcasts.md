---
title: Etiquetado de podcasts
subtitle: o como hacer que todo tenga sentido lexico
comments: false
---

## Paso 1. Operaciones con el archivo de audio

Identificar el archivo de audio con información importante así nunca se va a perder aunque viaje de ordenador en ordenador, de disco duro a disco duro, o de móvil a móvil por los siglos de los siglos amén.

### ¿Cómo se identifica un archivo de audio?

Mediante unas etiquetas que van incrustadas en mismo archivo que llevan por nombre ID3 Tags. Estas etiquetas almacenan en título del programa, la radio a la que pertenece, el año en que se emitió, otros detalles de interés e incluso imágenes.

A nivel más técnico podemos decir que ID3 es un estándar de metadatos para archivos sonoros (música, podcasts, etc)

### ¿Cómo edito los ID3?

Con un programa de edición, como por ejemplo ​ iTunes ​ para Mac o ​ EasyTag​ para otras plataformas: ​ https://wiki.gnome.org/Apps/EasyTAG​ . Por favor, mira el manual de estos programas para realizar el etiquetado. Es fácil.

Verás que en el programa aparecen varios campos para rellenar. Nosotros solamente vamos a usar algunos:

- **Titulo**:​Este es el nombre del episodio del podcast. Ejemplo: Historia del Reggaeton
- **Artista**:​Aquí hay que poner el nombre de tu radio. Ej: Ràdio Fabra
- **Album**:​Aquí va el nombre de tu programa. Ej: Black Market
- **Año**:Año de realización del programa.
- **Comentarios**:Puedes poner la dirección web de tu radio, tu programa o la licencia que
utilices.
- **Artwork**:​ pon aquí el logo de tu podcast o una foto representativa del show.

## Paso 2. Subir el podcast a la web de la XRCB

Una vez has ingresado a la web con tus credenciales puedes publicar un nuevo podcast a través del menú `+ > Afegeix > Podcast` o directamente por [este enlace](https://xrcb.cat/es/publish-podcast/).

Ahí se abrirá un formulario donde debes poner (casi) la misma información que usaste para las etiquetas ID3.

Para los detalles de la publicación, [mira este manual](../publicacion_de_podcasts).

## Paso 3. Identificar el archivo de audio

Sencillamente se trata de ponerle un nombre para identificarlo y poderlo archivar correctamente.

El criterio a seguir para la identificación de dicho archivo sería el siguiente: `Emisora_programa_nombre del podcast.mp3`

Ejemplos: `Ràdio Fabra_Black Market_Historia del Reggaeton.mp3`

Si cada uno de los programas no tiene un título específico puedes nombrar el archivo de la siguiente manera:

`Nombre Radio_Nombre programa_ fecha del programa.mp3`
`RadioFabra_Magazine_120319.mp3 (los números hacen referencia a la fecha de emisión).`

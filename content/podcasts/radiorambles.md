## Instruccions per la connectivitat d'internet a radiorambles

### Muntatge

- L'antena exterior va ubicada en un pal situat sobre del quiosc. Per fer el muntatge cal una escala. Un cop a dalt, desfer la protecció del cable i apuntar-la en direcció al palau de la virreina. Fer fixar la posició de l'antena s'ha d'enroscar el mecanisme, [vegi's secció *Montaje en poste*](https://dl.ubnt.com/qsg/NBE-5AC-Gen2/NBE-5AC-Gen2_ES.html).
  - Aquesta antena connecta amb un altre antena a pocs metres ubicada en la botiga *Música. Casa Beethoven*. Estan tant a prop que no cal ser gaire precís en l'alineament entre les antenes.
- A dins del quiosc, connectar l'enrutador i l'alimentación de l'antena externa (PoE de color blanc) a una regleta eléctrica. Al cap d'uns minuts hi hauria d'haver Internet a través de cable (preferible) i wifi. La connexió per cable es fa a través de qualsevol dels connectors taronges.
  - Verificació: comprovar que el botó del router (power switch) està encès.

### Desmuntatge

- Desconnexió elèctrica de l'enrutador i l'alimentació de l'antena externa. Això ens garanteix que els equips tindran major vida útil i no seran sensibles a tempestes.
- Desmuntatge de l'antena exterior, apretar mecanisme i desenroscar l'antena exterior, [Vegi's secció *Montaje en poste*](https://dl.ubnt.com/qsg/NBE-5AC-Gen2/NBE-5AC-Gen2_ES.html). Després, protegir el connector a dins d'una bosseta de plàstic i tancar-la amb cinta americana per evitar que pugui entrar aigua i s'oxidi el connector.

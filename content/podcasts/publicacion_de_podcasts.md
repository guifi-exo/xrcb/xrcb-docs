---
title: Publicación de podcasts
subtitle: 
comments: false
---

https://xrcb.cat/es/manual-podcast/

## Paso 1. Pre – tagueo: El nombre del archivo en el escritorio.

Antes de subir un podcast a la plataforma se debe nombrar de la siguiente forma: `Emisora_programa_nombre del podcast.mp3`

Ejemplos: `Ràdio Fabra_Black Market_Historia del Reggaeton.mp3`

Además debe tener las etiquetas ID3 correctamente asignadas. Se trata tanto de información como duración y codificación que se suele asignar automáticamente al codificar un mp3, como información útil como título y artista. Al añadir el archivo de audio en formato MP3 se extrae automáticamente esta meta información ID3 del archivo, lo que es necesario para que se reproduzca correctamente.

Los detalles del etiquetado te explicamos en [este manual](../etiquetado_de_podcasts).

## Paso 2. Rellenar formulario

La publicación de un podcast por parte de una radio se hace a través del menú `+ > Afegeix > Podcast` o directamente por [este enlace](https://xrcb.cat/es/publish-podcast/).

Hay que rellenar los siguientes campos:

   1. Pon un título a tu podcast
   2. Radio: Solamente aparece en caso que tengas varios radios asociados a tu cuenta
   3. Elige tu programa: Solamente aparece en caso que tengas la funcionalidad de programas activada
   4. Selecciona unas cuantas etiquetas
   5. ¿En qué idioma está?
   6. ¿De qué va tu podcast? descríbelo brevemente
   7. Elige tu archivo .mp3 y súbelo
   8. Confirmo que mi podcast cumple con las condiciones de uso de la XRCB.

![Formulario de publicación de podcasts](https://xrcb.cat/wp-content/uploads/2020/07/podcast-formulario.png)

## Paso 3. Publicación

Una vez publicado, la ficha del podcast aparece de la siguiente manera:

![Ficha del podcast publicado](https://xrcb.cat/wp-content/uploads/2020/07/podcast-publicado.png)

Si quieres editar un podcast ya existente, pinchas el enlace Edit al final de la ficha. Entonces volverás a ver el formulario del podcast con sus datos cargados:

![Ficha del podcast publicado](https://xrcb.cat/wp-content/uploads/2020/07/podcast-editar.png)

Todos los podcasts publicados se muestran de una forma reducida en el [listado de podcasts](https://xrcb.cat/es/llistat-podcasts/). En esta vista se puede filtrar los podcasts poniendo un término en el campo *Search*. Además se muestra un botón PLAY incrustado que permite reproducir el archivo de audio en el player.

![Listado de podcasts publicados](https://xrcb.cat/wp-content/uploads/2020/07/podcasts-listado.png)
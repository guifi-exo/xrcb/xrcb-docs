---
title: Alta Radio
subtitle: 
comments: false
---

## Notas preliminares

Para el correcto funcionamiento es importante que todos los contenidos de una radio estén dado de alta con el mismo usuario. Por lo tanto es necesario crear un usuario específico ANTES de dar de alta la radio y sus programas y podcasts.

## Alta usuario radio

La gestión de usuarios se hace en *Nextcloud* y no desde Wordpress. Para añadir un usuario hay que indentificarse en https://nuvol.xrcb.cat/ y seguir los siguientes pasos:

1. Abrir gestión de usuarios en https://nuvol.xrcb.cat/settings/users
2. Pinchar _+ New user_
3. Introducir los siguientes datos:
   - Username
   - Password
   - Email
   - Groups [xrcb.cat-radios]
   - Quota [Default quota]
4. Confirmar pinchando _Add a new user_

Ahora ya puedes acceder a Wordpress utilizando estos datos.

Recordar que los cambios de contraseña también se hace en https://nuvol.xrcb.cat/

## Alta nodo radio

Una vez creado el usuario se puede dar de alta la radio en *Wordpress*. Hay que identificarse en https://xrcb.cat/wp-admin y seguir los siguientes pasos:
1. Radios -> Afegeix (link directo: https://xrcb.cat/wp-admin/post-new.php?post_type=radio)
2. Rellenar los sigueintes campos:
   - Títol
   - Categoríes
   - Ciudad
   - Barrio
   - Seleccionar posición exacta en mapa, será el sitio donde aparezca luego la radio con una X sobre el mapa
   - Año de fundación
   - Breve historia en CAT, ES y EN
   - Licencia de uso
   - Podcast Image
   - Imatge destacada
3. Confirmar pinchando _Publica_

Ahora ya aparece la radio sobre el mapa en https://xrcb.cat y se abre su ficha pinchando la X. Puedes añadir podcasts para esta radio siguiendo los pasos de [Publicación de podcasts](../publicacion_de_podcasts).

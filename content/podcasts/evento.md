---
title: Como hacer un evento en directo
subtitle: 
comments: false
---

## 1. Reservar horario en calendario

### Opción A: sin ficha de podcast asociada

Es la vía más sencilla para reservar un espacio de streaming, pero no tiene una ficha de podcast asociada. Por lo tanto sirve para eventos que no tengan más información asociada.

Desde el backend de XRCB se va a *Afegeix* -> [Programació Live](https://xrcb.cat/programacio-live/) y se reserva el día y hora en el calendario. Además se pone el título y la duración del evento y se da click a `Enviar`. Se puede revisar la emisión creada en el calendario de la XRCB en https://xrcb.cat/ca/calendari/.

Se puede cambiar el título del evento posteriormente yendo al backend a [Programació](https://xrcb.cat/wp-admin/admin.php?page=easy_app_top_level), buscando el evento en cuestión y pinchar _Edit_.

### Opción B: Con ficha de podcast asociada (para evento de Radio Rambles)

Esta opción tiene la ventaja de tener una ficha de podcast asociada. En el caso de _Radio Rambles_ usamos siempre la misma ficha con la ventaja que mantengamos la URL https://xrcb.cat/radio-rambles-live/ que redirecciona a la URL del podcast https://xrcb.cat/es/podcast/radio-rambles-live/.

#### B.1 (Opcional) Crear ficha podcast para emisión en directo (no hace falta para evento de Radio Rambles)

Vas al formulario para crear un podcast desde el backend en [Podcasts afegeix](https://xrcb.cat/wp-admin/post-new.php?post_type=podcast) y rellenas los campos. Importante seleccionar la opción _Live Broadcast_ y añadir _Live Broadcast Duration_ en secundos.

![Crear Podcast LIVE](../../images/evento-live.png)

#### B.2 Reservar horario en calendario con ficha de podcast asociada

Desde el backend de XRCB vas a Afegeix -> [Programació Podcast](https://xrcb.cat/ca/programacio-de-emissions/) y reservas día y hora en el calendario. Además enlazas el podcast creado en el paso anterior y pinchas `Enviar`. Puedes revisar la emisión creada en el calendario de la XRCB en https://xrcb.cat/ca/calendari/.

##### Reservar horario en calendario el mismo día del evento

Reservar el horario en el calendario hay que hacer por lo menos con un día de antelación. En caso que se quiera hacer el mismo día del evento, entonces hay que editar una programación creada. Eso se hace a través del menú [Programación](https://xrcb.cat/wp-admin/admin.php?page=easy_app_top_level) del backend. La forma más fácil es:

1. Buscar el evento/los eventos que actualmente ocupan el horario de la emisión y borrarlos.
2. Editar el tiempo Programació > Settings > Date & Time > Block time y poner valor 0.
3. Reservar el horario en calendario como explicado en https://guifi-exo.gitlab.io/xrcb/xrcb-docs/podcasts/evento/#reserver-horario-en-calendario
4. Resetear el valor del Block time del punto 2. a 1000

## 2. Anunciar evento en esdeveniments (opcional)

Puedes anunciar tu evento en el apartado [esdeveniments](https://xrcb.cat/ca/category/event/) rellenando [este formulario](https://xrcb.cat/wp-admin/post-new.php). Importante selección la categoría `esdeveniments`.

Nota: El próximo evento será anunciado automáticamente en el slider de la portada como "último evento".

Para que aparezca un enlaze al player, puedes insertar este código en la entrada, pestaña `Text`:

`🔴 <a href="https://xrcb.cat/radio-rambles-live"><strong>Click to LIVE</strong></a> 16.15-17:45 h.`

![Enlace player evento](../../images/event-player.png)

Nota: En la ficha de la radio (solamente Radio Ramblas y Radio Fabra) aparece automáticamente el player con el Live durante el horario anunciado en el calendario:

![Player live Radio](../../images/event-player-radio.png)

## 3. Anunciar evento en slider (opcional)

Puedes anunciar tu evento en el slider de la portada de XRCB rellenando [este formulario](https://xrcb.cat/wp-admin/post-new.php?post_type=slide). Recuerda marcar la categoría "Evento" para que aparezca en la portada con la etiqueta "evento". Opcionalmente puedes enlazar el podcast o el evento o cualquier web con el anuncio creado.

Nota: El próximo evento publicado en el apartado [esdeveniments](https://xrcb.cat/ca/category/event/) será anunciado automáticamente en el slider de la portada como "último evento". Por lo tanto solamente hay que efectuar punto 2 o 3, pero nunca las 2 a la vez para un mismo evento.

## 4. Streaming

En esta sección se incluyen detalles sobre cómo lanzar un streaming a la plataforma xrcb.cat. El streaming es captado a través de un programa que se llama liquidsoap y que funciona como si fuera un icecast/shoutcast. La única diferencia entre el streaming de pruebas y en producción es el puerto, 8002 para pruebas y 8001 para emitir (y se interrumpe la programación programada)

La contraseña de momento es maestra y en el futuro cada usuaria tendrá la suya

Se recomienda probar el streaming antes de lanzar directo para evitar problemas de conectividad (filtrado de puertos, problemas de configuración)

A veces puede ser problemático subir más el volumen desde donde se realiza la emisión, en tales casos, se puede subir el volumen desde el servidor. Esta opción de momento solo la puede gestionar personas con acceso a la terminal del servidor

- streaming de pruebas
  - bitrate: 256 Kbps
  - server type / protocol: icecast
  - server address: xrcb.cat
  - user: source
  - port: 8002
  - password: *a solicitar*
  - mountpoint: live2-input.mp3
  - player: https://icecast.xrcb.cat/live2-test.mp3

- producción (sale en player general):
  - bitrate: 256 Kbps
  - server type: icecast
  - server address: xrcb.cat
  - user: source
  - port: 8001
  - password: *a solicitar*
  - mount: live2-input.mp3
  - player: https://icecast.xrcb.cat/main.mp3

### Dimensionar coste streaming en datos de cobertura móvil

Si el sitio donde vas a hacer un evento no tiene conectividad a internet y te la tienes que proveer tú mismo, quizá estás considerando en usar un pack de datos móviles

El ancho de banda necesario serían 256 Kbps por hora, que en MegaBytes por hora, serían 115,2 MB por cada hora, la fórmula utilizada es: `256*3600/(1000*8)`

### Grabación temporal de respaldo del streaming en servidor

Todo lo que se emite en directo es grabado en estructura de ficheros de 30 min y almacenado por 3-4 días. Si necesitas recuperar alguna transmisión, ponte en contacto con el equipo técnico de xrcb.cat

## 5. Postproducción

Para publicar el evento solamente hace falta subir el mp3 grabado a la ficha del podcast del paso 1 y modificar el tipo de `Live Broadcast` a `Podcast`. A partir de este momento saldrá el evento en el listado de podcasts de tu radio y en el [listado general de podcasts de la XRCB](https://xrcb.cat/ca/llistat-podcasts/).

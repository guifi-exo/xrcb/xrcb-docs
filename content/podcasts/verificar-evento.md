---
title: Cómo verificar un evento en directo
subtitle: 
comments: false
---

## protocolo live

Si ocurre algún problema, anotar especialmente día-hora de cuándo ha ocurrido, y qué ha pasado con el mayor detalle posible

### comprobaciones de que el evento va bien

#### evento

- El evento del día está en portada como "último evento" en el slider
- En el evento en cuestión tiene un enlace al player https://guifi-exo.gitlab.io/xrcb/xrcb-docs/podcasts/evento/

#### streaming

- Calendario xrcb.cat https://xrcb.cat/en/calendari/
  - Que en el calendario estén los eventos programados
  - Nota: no se puede modificar la programación en el mismo día. A las 23:00 del día anterior se programa el día siguiente. Por lo tanto, coordinarse en días anteriores para garantizar una correcta programación.
- icecast.xrcb.cat
  - "Mount Point /live2.mp3" esté en activo en el momento de la transmisión
  - En caso de consultarlo en momentos posteriores, la hora de inicio del streaming debería encajar con la hora programada, ejemplo: `Stream started:	Wed, 06 Apr 2022 17:00:11 +0200`

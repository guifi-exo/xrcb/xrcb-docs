---
title: Radios y Podcasts
subtitle: cómo publicar y escucar programas
weight: 20
comments: false
---

###  ¿haces podcast / radio? 
#### [Etiquetar podcast]({{< ref "etiquetado_de_podcasts.md" >}})
#### [Publicar podcast]({{< ref "publicacion_de_podcasts.md" >}})
#### [Publicar repost]({{< ref "publicacion_de_repost.md" >}})

###  ¿escuchas podcast / radio?
#### [Suscribirse a un podcast]({{< ref "subscripcion_podcast.md" >}})

###  ¿quieres comunicarte con XRCB y sus radios?
#### [Chat de XRCB]({{< ref "chatoforo.md" >}})
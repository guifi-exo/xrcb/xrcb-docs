---
title: Chat de XRCB
subtitle: 
comments: false
---

## Chat de XRCB

### Conectarse a través de Nextcloud

Hay una interfaz web dentro de nuestro Nextcloud en https://nuvol.xrcb.cat/

A través del menú se puede conectar a Element, la URL directa es https://nuvol.xrcb.cat/apps/riotchat/

### Conectarse a través de App Externa

Hay que poner los siguientes datos para conectarse:

- Server: https://xrcb.cat
- User: el mismo usuario usuario de xrcb.cat (Wordpress) o nuvol.xrcb.cat (Nextcloud)
- Password: la misma contraseña de xrcb.cat (Wordpress) o nuvol.xrcb.cat (Nextcloud)

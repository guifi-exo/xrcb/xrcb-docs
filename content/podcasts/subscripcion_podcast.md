---
title: Subscripción a podcast
subtitle: 
comments: false
---

## Subscripción desde feed RSS

XRCB permite subscribirse tanto a una radio como a un programa de una radio. Por lo tanto la radio (o el programa) sería el equivalente a un podcast en la terminología popular.

La forma universal para subscribirse a un podcast de la XRCB es copiar y pegar el enlace RSS de una radio o un programa al reproductor de podcasts. A partir de este momento reciberá todas las actualizaciones de la radio o del programa.

Un ejemplo de un feed RSS de La Comunitaria: `https://xrcb.cat/subscribirse-a-podcast/?id=315`

## Directorios de podcasts

Apps populares de podcasts - como por ejemplo Itunes o Podcast Addict - usan directorios de podcasts para alimentar su buscador. Los directorios de podcasts más populares - como Itunes, Spotify o TuneIn Radio - no nos gustan por su espírito cerrado y abusivo. Por un lado exigen un usuario para comunicar podcasts a su sistema y además viven del negocio con nuestros datos.

Por eso por ahora comunicamos nuestros podcasts solamente a directorios con un espíritu abierto y comunitario como https://gpodder.net, https://fyyd.de/ y https://podcastindex.org/

## Subscripción desde AntennaPod

### Feed RSS

En el caso de AntennaPod se puede pegar cualquier Feed RSS al campo *Add Podcast to URL*.

![Add Podcast to AntennaPod with Feed RSS](../../images/subscribe-AntennaPod1.jpg)

### Directorio

AntennaPod tiene integrado búsquedas en los directorios de gpodder y fyyd, aquí unos ejemplos con los podcasts de la XRCB:

![Add Podcast to AntennaPod with gpodder.net](../../images/subscribe-AntennaPod2.jpg)
![Add Podcast to AntennaPod with fyyd](../../images/subscribe-AntennaPod3.jpg)
![Add Podcast to AntennaPod final](../../images/subscribe-AntennaPod4.jpg)

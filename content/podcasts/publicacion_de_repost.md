---
title: Publicación de repost
subtitle: 
comments: false
---

Podcasts ya publicadas por otros radios pueden ser enlazadas desde cualquier radio. Con eso se consigue que contenidos publicadas en otra radio aparezcan también en nuestra radio.

Para conseguir un repost, sigue los siguientes pasos:

   1. Abre el formulario para hacer el repost navegando desde el backend a Reposts -> Afegeix o directamente a https://xrcb.cat/wp-admin/post-new.php?post_type=repost Deberías ver el siguiente formulario:
   ![Crear un repost](../../images/xrcb-create-repost.png)

   2. Busca el podcast relacionado en el campo _Podcast_ Sale un listado con todos los podcasts publicados en XRCB y por lo tanto es un listado muy largo. Se puede filtrar el listado escribiendo una o varias palabras del título.
   3. Pinchar botón _Publica_
   4. Controla el resultado navegando a la ficha de la radio, debería aparecer igual que cualquier otro podcast, pero con el ícono de _repost_ (flechas circulares) al lado:
   ![Resultado de un repost](../../images/xrcb-repost-result.png)